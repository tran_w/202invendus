#!/usr/bin/env python3

# 202invendus - EPITECH 2016

import sys
from math import *

X = [x for x in range(10, 60, 10)]
Y = X
Z = [z for z in range(20, 110, 10)]

# Check if given parameters are integers and higher than 50
def checkArgs(a, b):
	if a.isdigit() and b.isdigit():
		if int(a) > 50 and int(b) > 50:
			return True
		else:
			raise RuntimeError('Integers must be higher than 50.')
	raise RuntimeError('Arguments must be integers.')
	return False

# Using the joint probability distribution
def jointDistribution(a, b):
	results = list()

	for y in Y:
		for x in X:
			result = ((a - x) * (b - y)) / ((5 * a - 150) * (5 * b - 150))
			results.append(result)
	return (results)

# Get Y from marginal distribution method
def marginalDistributionY(results):
	resultsY = list()
	resultsSum = 0
	cnt = 1

	for result in results:
		resultsSum += result
		if cnt % len(Y) == 0:
			resultsY.append(resultsSum)
			resultsSum = 0
		cnt += 1
	return resultsY

# Get X from marginal distribution method
def marginalDistributionX(results):
	resultsX = [0 for x in range(5)]
	cnt = 0

	for result in results:
		if cnt == len(X):
			cnt = 0
		resultsX[cnt] += result
		cnt += 1;
	return resultsX

# Using the probability distribution
def probabilityDistributionZ(a, b):
	resultsZ = dict()
	# Initialyze the dictionary
	for z in Z:
		resultsZ[z] = 0

	for y in Y:
		for x in X:
			resultsZ[x+y] += ((a - x) * (b - y)) / ((5 * a - 150) * (5 * b - 150))
	return (resultsZ)

# Calculate the expected value
def calculateExpectedValue(a):
	expectedValue = 0

	for x in X:
		expectedValue += (x * ((a - x) / ((5 * a) - 150)))
	return expectedValue

# Calculate variance
def calculateVariance(a, expectedValue):
	variance = 0

	for x in X:
		variance += pow(x, 2) * ((a - x) / ((5 * a) - 150))
	variance -= pow(expectedValue, 2);
	return variance

def convertToString(number):
	numberConverted = str(round(number, 3)).rstrip('0')
	numberLength = len(numberConverted)
	if numberConverted[numberLength - 1] == '.':
		numberConverted = numberConverted.replace(".", "")
	return numberConverted

# Print the result of the marginal distribution
def showResults(a, b):
	results = jointDistribution(a, b)
	resultsX = marginalDistributionX(results)
	resultsY = marginalDistributionY(results)
	resultsZ = probabilityDistributionZ(a, b)

	# Show head
	print("\n\t", end="")
	for x in X:
		print("\tX = %d" % x, end="")
	print("\tLoi de Y\t")

	# Show the marginal distribution Y's result and joint distribution
	i = 0
	cnt = 0

	for result in results:
		if cnt % len(Y) == 0:
			print("Y = %d\t" % Y[i], end="")
		cnt += 1
		print("\t%s" % convertToString(result), end="")
		if cnt % len(Y) == 0:
			print("\t%s" % convertToString(resultsY[i]))
			i += 1

	# Show the marginal distribution X's result
	print("Loi de X", end="")
	for resultX in resultsX:
		print("\t%s" % convertToString(resultX), end="")
	print("\t%d" % 1)

	# Show the probability distribution's result and calculate the total
	total = 0

	print("\nz\t\t", end="")
	for z in Z:
		print("%d\t" % z, end="")
	print()
	print("p(Z=z)\t\t", end="")
	for z in Z:
		print("%s\t" % convertToString(resultsZ[z]), end="")
		total += resultsZ[z]
	print()
	print("Total\t\t%.0f" % total)

	# Show expected value and variance
	print()
	# For X
	expectedValue = calculateExpectedValue(a)
	variance = calculateVariance(a, expectedValue)

	print("Espérance de X : \t%s" % convertToString(expectedValue))
	print("Variance de X : \t%s" % convertToString(variance))
	# For Y
	expectedValue = calculateExpectedValue(b)
	variance = calculateVariance(b, expectedValue)

	print("Espérance de Y : \t%s" % convertToString(expectedValue))
	print("Variance de Y : \t%s" % convertToString(variance))
	# For Z
	expectedValue = 0
	variance = 0

	for z in range(20, 110, 10):
		expectedValue += z * resultsZ[z]
		if (z >= 20):
			variance += pow(z, 2) * resultsZ[z]
	variance -= pow(expectedValue, 2)

	print("Espérance de Z : \t%s" % convertToString(expectedValue))
	print("Variance de Z : \t%s" % convertToString(variance))

# Main
def main():
	try:
		if len(sys.argv) != 3:
			print("Usage: ./202invendus <a> <b>")
		else:
			if checkArgs(sys.argv[1], sys.argv[2]) is True:
				showResults(int(sys.argv[1]), int(sys.argv[2]))
		return True
	except Exception as err:
 		sys.stderr.write("ERROR: %s\nProgram is exiting now.\n" % str(err))
 		return False

if __name__ == '__main__':
	sys.exit(main())